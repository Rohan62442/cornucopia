#Cornucopia Icon Set, v1.0
-----------------------

This icon set is licensed under the GNU General Public license, version 3, a copy of which is provided with the set. 

This icon set is a COMPILATION of Ubuntu icons from various sources, primarily the FS Icon set by Frank Souza. 

http://franksouza183.deviantart.com/art/FS-Icons-Ubuntu-288407674

http://franksouza183.deviantart.com/art/FS-Icons-288489126

The above icon sets were also hosted on gnome-look.org .

##Installation:
Extract the archive to either of the following locations:

	/home/username/.icons

	/usr/share/icons

Use Unity Tweak tool or Gnome Tweak tool to set the icons to Cornucopia-1.0.

You are free to contact me for any queries, help, advice, suggestions etc. Definately need suggestions.

##Author:
Rohan Pinto

Email: rohan62442@hotmail.com